#!/bin/bash

mkdir -p flatpak-data
XDG_DATA_HOME="$PWD/flatpak-data" exec flatpak "$@"
