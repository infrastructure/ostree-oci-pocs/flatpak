from asyncio import subprocess as asubprocess

import json

from starlette.applications import Starlette
from starlette.background import BackgroundTask
from starlette.requests import Request
from starlette.responses import Response, StreamingResponse
from starlette.routing import Route

import gitlab
import httpx


REGISTRY = 'registry.gitlab.apertis.org'
# XXX: hardcoded port
PROXY = 'localhost:8000'


gitlab_instance = gitlab.Gitlab(url='https://gitlab.apertis.org')

client = httpx.AsyncClient(
    base_url=f'https://{REGISTRY}/v2/',
    follow_redirects=True,
)


class DictFilters:
    def __init__(self, query_params: dict[str, str], prefix: str) -> None:
        assert prefix.endswith(':'), prefix
        self.filter_present = set()
        self.filter_values = {}
        self.filter_desc = []

        for param, value in query_params.items():
            if param.startswith(prefix):
                self.filter_desc.append(f'{param}={value}')

                key = param.removeprefix(prefix)
                if key.endswith(':exists'):
                    self.filter_present.add(key.removesuffix(':exists'))
                else:
                    sefl.filter_values[key] = value

    def matches(self, values: dict[str, str]) -> bool:
        for key in self.filter_present:
            if key not in values:
                return False

        for key, value in self.filter_values.items():
            if key not in values or values[key] != value:
                return False

        return True


async def get_index(request: Request) -> Response:
    results = []

    filter_tags = set(request.query_params.getlist('tag'))
    filter_os = request.query_params.get('os', '')
    filter_arch = request.query_params.get('architecture', '')
    filter_annotations = DictFilters(request.query_params, 'annotation:')
    filter_labels = DictFilters(request.query_params, 'label:')

    project = gitlab_instance.projects.get(
        'infrastructure/ostree-oci-pocs/flatpak'
    )
    for repo in project.repositories.list():
        # XXX: doesn't filter by repository
        images_to_inspect = set()
        for tag in repo.tags.list():
            details = repo.tags.get(tag.name)
            images_to_inspect.add(f'{repo.location}@{details.digest}')

        repo_images = []

        for image in images_to_inspect:
            p = await asubprocess.create_subprocess_exec(
                'skopeo',
                'inspect',
                f'docker://{image}',
                stdout=asubprocess.PIPE,
            )
            stdout, stderr = await p.communicate()
            if p.returncode:
                print(
                    f'skopeo on {image} failed with exit code {p.returncode}'
                )
                continue

            info = json.loads(stdout)
            if set(info['RepoTags']) & filter_tags != filter_tags:
                print(
                    f'image: {info["RepoTags"]} does not match {filter_tags}'
                )
                continue
            if filter_os and info['Os'] != filter_os:
                print(f'{image}: {info["Os"]} does not match {filter_os}')
                continue
            if filter_arch and info['Architecture'] != filter_arch:
                print(
                    f'{image}: {info["Architecture"]} does not match {filter_arch}'
                )
                continue
            if not filter_annotations.matches(info.get('Annotations', {})):
                print(
                    f'{image}: annotations do not match {filter_annotations.filter_desc}'
                )
                continue

            repo_images.append(
                {
                    'Tags': info['RepoTags'],
                    'Digest': info['Digest'],
                    'Architecture': info['Architecture'],
                    'OS': info['Os'],
                    'Annotations': info.get('Annotations', {}),
                    'Labels': info.get('Labels', {}),
                }
            )

        if repo_images:
            results.append(
                {
                    'Name': PROXY + repo.location.removeprefix(REGISTRY),
                    'Images': repo_images,
                    'Lists': [],
                }
            )

    return Response(
        content=json.dumps(
            {
                'Registry': f'http://{PROXY}/',
                'Results': results,
            }
        ),
        media_type='application/json',
    )


async def forward_to_gitlab(request: Request) -> Response:
    req = client.build_request(
        request.method,
        # XXX: why does this end up here?? might be a Flatpak bug because of
        # having a colon in the name...
        request.path_params['path'].replace(f'{PROXY}/', ''),
        params=request.query_params,
        headers=[
            (header, value)
            for header, value in request.headers.items()
            if header not in {'host', 'accept-encoding', 'connection'}
        ],
    )
    print(req.url, req.headers)
    resp = await client.send(req, stream=True)
    print(resp, resp.headers)
    return StreamingResponse(
        resp.aiter_bytes(),
        headers={
            header: value
            for header, value in resp.headers.items()
            if header not in {'transfer-encoding', 'content-encoding'}
        },
        media_type=resp.headers['content-type'],
        status_code=resp.status_code,
        background=BackgroundTask(resp.aclose),
    )


app = Starlette(
    debug=True,
    routes=[
        Route('/index/static', get_index, methods=['GET']),
        Route('/v2/{path:path}', forward_to_gitlab, methods=['HEAD', 'GET']),
    ],
)
