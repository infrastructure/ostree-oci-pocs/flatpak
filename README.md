# flatpak-oci-test

This repo contains a simple example of using Flatpaks from GitLab's container
registry.

- `push-flatpak.sh` downloads Apertis's headless Flatpak runtime and uploads
  it to GitLab Container Registry.
- `server.py` is a demo server that implements the [flagstate
  protocol](https://github.com/owtaylor/flagstate/blob/master/docs/protocol.md),
  backed by GitLab's API that proxies OCI distribution requests back to the
  container registry. It can be run via `poetry install` +
  `poetry run uvicorn server:app`.
  - Some parts of the protocol not needed by Flatpak (such as fitering by
    repository, the `/index/dynamic` endpoint, and support for multiarch /
    image lists) were not implemented.
  - Due to being a simple PoC, there are several notable inefficiencies, such
    as rebuilding the entire index every request and shelling out to skopeo to
    inspect OCI images.
- `isolated-flatpak.sh` runs Flatpak with an isolated config directory, for
  testing. This was used to install the runtime from GitLab's container
  registry (assuming the server is already running):
  ```
  $ ./isolated-flatpak.sh --user remote-add test oci+http://localhost:8000
  $ ./isolated-flatpak.sh --user install test org.apertis.headless.Platform
  ```

## Known Issues

- `remote-info` doesn't work
  - GitLab's OCI registry wants auth for this endpoint, but Flatpak never sets
    it up (Flatpak bug?)
- Can't create static deltas: https://github.com/containers/image/pull/902
- No signature support at all:
  https://github.com/flatpak/flatpak/commit/d6ea398dd56f8f65578ca5ed4a16fe84301de521
