#!/bin/bash

set -ex

mkdir -p repo
ostree --repo=repo init --mode=archive-z2
ostree --repo=repo remote add --if-not-exists --no-gpg-verify apertis \
  https://images.apertis.org/flatpak/repo
# Retry the pull a few times if it fails.
for i in $(seq 10); do
  ostree --repo=repo pull apertis \
    runtime/org.apertis.headless.Platform/$(uname -m)/v2024dev1 \
    && break \
    || :
done
flatpak build-bundle --oci --runtime --arch=$(uname -m) repo/ image \
  org.apertis.headless.Platform v2024dev1
skopeo -v copy oci:image \
  docker://registry.gitlab.apertis.org/infrastructure/ostree-oci-pocs/flatpak/org.apertis.headless.platform
